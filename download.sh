#!/bin/bash
set -euo pipefail

function usage {
    echo "usage: download.sh <parameter_profile_file> <target_dir>"
    exit 1
}

if [ $# -ne 2 ]; then
    usage
fi
PARAMETER_PROFILE_FILE=$1
TARGET_DIR=$2

if [ ! -f $PARAMETER_PROFILE_FILE ]; then
    echo "Parameter profile file not found: $PARAMETER_PROFILE_FILE"
    usage
fi


if [ ! -d $TARGET_DIR ]; then
    echo "Target dir not found: $TARGET_DIR"
    usage
fi

echo "download data to target dir: $TARGET_DIR"

# From https://www.data.gouv.fr/fr/datasets/donnees-carroyees-a-200m-sur-la-population/
DATASET_URL='https://www.data.gouv.fr/fr/datasets/r/6072929c-ba60-4a15-b797-ee3c1b20e21b'
ZIP_FILE_NAME=carroyage_insee_metro.zip

cd $TARGET_DIR
wget -q $DATASET_URL -O $ZIP_FILE_NAME
unzip $ZIP_FILE_NAME
echo "downloaded Insee population"

# Download cycles GeoJSON file
TERRITORY=`jq -r '.territory' $PARAMETER_PROFILE_FILE`
echo "TERRITORY: $TERRITORY"

REQ="https://api.openmobilityindicators.org/indicator-data?parameter_profile="$TERRITORY"&slug=cycles&sort=created_at%3Adesc&page=1&size=1"

wget -q "$REQ" -O $TARGET_DIR/response
RAWID=`jq -r '.indicator_data| .items| .[0]| .id' $TARGET_DIR/response`
echo "RAWID: $RAWID"

CYCLES_URL="https://files.openmobilityindicators.org/indicator-data/"$RAWID"/"ped_network_blocks.geojson
echo "CYCLES_URL: $CYCLES_URL"

echo "download ped network blocks geojson file to target dir: $TARGET_DIR"
wget -q "$CYCLES_URL" -O ped_network_blocks.geojson
echo "done."


echo "done."
