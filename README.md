# OMI notebook indicator : "Population density from cycles" 
## description : [indicator.yml FILE](https://gitlab.com/open-mobility-indicators/indicators/population-density-from-cycles/-/blob/main/indicator.yaml)

Calcul des populations des ilots formés par la voirie piétonne ("cycles" du réseau, ou "pâtés de maison") à partir du fichier des cycles déjà calculés ped_network_blocks.geojson généré par l'indicateur OMI [cycles](https://gitlab.com/open-mobility-indicators/indicators/cycles/)

Le fichier produit `pop_density.geojson` peut être ensuite réutilisé pour calcul l'indicateur de marchabilité ["mon quartier à pied"](https://gitlab.com/open-mobility-indicators/indicators/mon-quartier-a-pied/).
(l'infrastructure OMI produit ensuite des tuiles vecteur .mbtiles à partir du fichier geoJSON).   
Pour chaque ilot, en plus de la géométrie, le fichier contient les attributs suivants : 
- 'pop' : population
- 'perimetre' : block perimeter lenght in meters
- 'densite' : habitants / km2 de l'ilot

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute)](https://gitlab.com/open-mobility-indicators/website/-/wikis/2_contributeur_technique/install-a-notebook-locally)